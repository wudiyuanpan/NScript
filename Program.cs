﻿using BSF.BaseService.NScript.Compiler;
using BSF.BaseService.NScript.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BSF.BaseService.NScript
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            //Application.Run(new FrTest());
            //return;
            //args = new string[] {"/run","a.cs" };
            AssemblyHelper.RegisterAssemblyFind();

            if (args == null||args.Length == 0)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new FrCodeEdit());
                return;
            }
            string cmd = args[0].ToLower().Replace("/","");
            if (cmd == "run")
            {
                try
                {
                   var result = new MainCompiler().DoCompiler(new Core.CompilerInfo().Parse( Core.EnumSourceType.File, args[1]));
                    var newargs = new List<string>(args); newargs.RemoveRange(0,2);
                    AppDomain.CurrentDomain.ExecuteAssembly(result.Assembly.Location,newargs.ToArray());
                }
                catch(Exception exp)
                {
                    MessageBox.Show(exp.Message);
                }
                return;
            }
            if (cmd == "help")
            {
                string msg = StringResources.ExeHelp;
                MessageBox.Show(msg);
                return;
            }
            MessageBox.Show("命令未识别,/help 查看命令");
        }
    }
}
