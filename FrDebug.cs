﻿using BSF.BaseService.NScript.Compiler;
using BSF.BaseService.NScript.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BSF.BaseService.NScript
{
    public partial class FrDebug : Form
    {
        public FrDebug()
        {
            InitializeComponent();
            this.cbCompilerMode.SelectedIndex = 0;
        }

       // public BSF.BaseService.NScript.FrCodeEdit.FileInfoItem MainFileInfoItem = null;
        public FrCodeEdit FrParent;
    

        private void btnCall_Click(object sender, EventArgs e)
        {
            try
            {
                //删除临时文件
                FrParent.DeleteCompilerTempFiles();
                //保存
                FrParent.SaveFileCode();
                //编译
                EnumCompilerMode compilerMode = (EnumCompilerMode)Enum.Parse(typeof(EnumCompilerMode), this.cbCompilerMode.Text.Trim(), true);
                FrParent.RunCompiler(compilerMode);
                //调试
                string compilertempMainFilePath = FrParent.GetMainCompilerTempFile();

                if (compilerMode == EnumCompilerMode.Main)
                {
                    //var result = NScriptHelper.RunCompiler(new CompilerParams()
                    //{
                    //    EnumSourceType = EnumSourceType.File,
                    //    CodeOrFileName = compilertempMainFilePath,
                    //    EnumCompilerMode = compilerMode
                    //});
                    System.Diagnostics.Process p = new System.Diagnostics.Process();
                    p.StartInfo.FileName = "cmd.exe";//要执行的程序名称
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.RedirectStandardInput = true;//可能接受来自调用程序的输入信息
                    p.StartInfo.RedirectStandardOutput = true;//由调用程序获取输出信息
                    p.StartInfo.CreateNoWindow = true;//不显示程序窗口
                    p.Start();//启动程序 
                    p.StandardInput.WriteLine(System.Windows.Forms.Application.ExecutablePath + " /run " + compilertempMainFilePath);
                    this.BeginInvoke(new Action(()=>{
                        System.Threading.Thread.Sleep(1000);
                        FrParent.DeleteCompilerTempFiles();
                    }), null);
                   
                    //AppDomain.CurrentDomain.ExecuteAssembly(result.Assembly.Location, new string[]{});
                }
                else
                {
                    CompilerResult result = null;
                    var obj = NScriptHelper.Run<Object>(new CompilerParams()
                    {
                        EnumSourceType = EnumSourceType.File,
                        CodeOrFileName = compilertempMainFilePath,
                        EnumCompilerMode = compilerMode
                    }, this.tbClassPath.Text.Trim(), this.tbMethodName.Text.Trim(),
                        new Object[] { }, out result);
                    FrParent.DeleteCompilerTempFiles();
                    MessageBox.Show(obj == null ? "" : obj.ToString());
                }
            }
            catch(Exception exp)
            {
                FrParent.DeleteCompilerTempFiles();
                if (exp is NScript.Compiler.NScriptException)
                    MessageBox.Show("调用出错:" + (exp as NScriptException).MessageDetail);
                else
                    MessageBox.Show("调用出错:" + exp.Message);
            }
        }

        private void FrDebug_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }
    }
}
