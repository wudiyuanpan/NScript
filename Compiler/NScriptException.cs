﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSF.BaseService.NScript.Compiler
{
    public class NScriptException : Exception
    {
        public string MessageDetail
        {
            get {
            string message = (this.Message==null?"":this.Message);
            Exception exp = null;
            exp = this.InnerException;
            while(exp !=null)
            {
                message += "\n" + (exp.Message == null ? "" : exp.Message);
                exp = exp.InnerException;
            }
            return message;
        }
        }
        public NScriptException(string message)
            : base(message)
        {

        }
        public NScriptException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
