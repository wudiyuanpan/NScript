﻿using BSF.BaseService.NScript.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace BSF.BaseService.NScript.Compiler
{
    public class AssemblyCompiler : BaseCompiler
    {
        private static CompilerCache cache = new CompilerCache();

        public override Core.CompilerResult DoCompiler(CompilerInfo compilerInfo)
        {
            compilerInfo.EnumCompilerMode = EnumCompilerMode.Assembly;
            return cache.GetOrAdd(compilerInfo.GetHashCode(), () =>
            {
                var assembly = new DynamicCompilerProvider().Compiler(compilerInfo);

                var result = new CompilerResult();
                result.EnumCompilerMode = EnumCompilerMode.Assembly;
                result.Assembly = assembly;
                result.AppDomianInfo = null;

                return result;

            });
        }
    }
}
