﻿using BSF.BaseService.NScript.Compiler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace BSF.BaseService.NScript.Core
{
    /// <summary>
    /// 编译结果
    /// </summary>
    public class CompilerResult:IDisposable
    {
        public Assembly Assembly;
        public AppDomianInfo AppDomianInfo;
        public EnumCompilerMode EnumCompilerMode;

        public void Dispose()
        {
            if (this.AppDomianInfo != null)
            {
                this.AppDomianInfo.Dispose();
            }
        }
    }

    public class AppDomianInfo:IDisposable
    {
        public AppDomain AppDomain;
        public AppDomainAssemblyLoader AppDomainAssemblyLoader;

        public void Dispose()
        {
            if (AppDomain != null)
            {
                try
                {
                    
                    System.AppDomain.Unload(this.AppDomain);
                    this.AppDomain = null;
                }
                catch (Exception exp)
                { 
                }
            }
        }
    }
}
